#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>

#define SIZE 1024*1024*1024ULL
//#define SIZE 1024*1024*64ULL
#define BANK_MASK 0b111111110000000000000
#define ROW_MASK 0b1111111111111100000000000000000
#define ROW_START 17
#define BANK_NUM 8
#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b;})


uint64_t BANK_BIT[4] = {(1<<13) + (1<<17), (1<<14) + (1<<18), (1<<15) + (1<<19), (1<<16) + (1<<20)};
int latency_regularity[16][8] = {0};


int ab, cd, ef, gh;


int pagemap;
//uint64_t PT[4][262144] = {0}; // 0 - PA, 1 - VA, 2 - cont, 3 - latency
uint64_t PT[4][16384] = {0};
uint64_t pt[2][262144] = {0};
//uint64_t PFN_check[262144] = {0};
int Bruler[1024] = {0};
int Rruler[1024] = {0};

int detect_contiguous_pages(int , int);
uint64_t convert_VA_into_PFN(uint64_t);
int print_contiguity(int, int);
void init_PT(char *, uint64_t);
void print_PFN(void);
int check_interval(int);
int get_bank(uint64_t);
int get_row(uint64_t);
unsigned long time_stamp();
int check_latency(uint64_t VA1, uint64_t VA2);
int single_check_latency(uint64_t VA);
void check_array();
int init_cont(int, int);
char *get_va(int row, int bank, char *addr, int size)
{
    int i;
    char *temp;
    for(i = 0; i < SIZE / 4096; i++)
    {
        uint64_t pa = pt[0][i];
        if(get_row(pa) == row && get_bank(pa) == bank)
            return (char *)pt[1][i];
    }

    return 0;
}

void init_real_pt(char *addr, int size)
{
    char *temp;
    int i = 0;
    for(temp = addr; temp < addr + size; temp += 4096){
        *temp = 1;
        pt[0][i] = convert_VA_into_PFN((uint64_t)temp) << 12;
        pt[1][i] = (uint64_t)temp;
        i++;
    }
//    printf("IN INIT - uint64_t va : %"PRIx64", va : %p\n", (uint64_t)addr/*PT[1][0]*/, addr);
}

int init_cont(int size, int threshold)
{
    int i, j, cont = 1, num = 0;
    for(i = 1; i < size; i++)
    {
        if((PT[0][i-1] + 1)  == PT[0][i])
            cont++;
        else
        {
            if(cont >= threshold)
                for(j = 0; j <= cont; j++)
                {
                    PT[2][i - j/* - 1*/] = 1;
                }
            num++;
            cont = 1;
        }
    }

    for(i = 0; i < size; i++)
    {
        if(check_latency(PT[1][0], PT[1][i]) > 700)
            PT[3][i] = 1;
        else
            PT[3][i] = 0;
//        PT[3][i] = check_latency(PT[1][0], PT[1][i]);
    }
    return num;
}
void make_ruler() // depends on # of BANK
{
    int size = 1 << BANK_NUM;
    int bank_size = 1 << (BANK_NUM/2);
    int Btemp[16][32] = {0};
    int Rtemp[16][32] = {0};
    int addr;
    int index[16] = {0};

/*
    for(addr = 0; addr < size * 2; addr++)
    {
        uint64_t addr_ = (uint64_t)(addr<<12);
        int bank = get_bank((uint64_t)(addr_));
        int row = get_row((uint64_t)(addr_));
        Btemp[bank][index[bank]] = addr;
        index[bank]++;
    }


    int i, j, k;


    for(k = 0; k < 2; k++)
    {
        for(i = 0; i < 16; i++)
        {
            for(j = 0; j < 32; j++)
            {
                Bruler[Btemp[i][j] + 512 * k] = i;
            }
        }
    }

    for(i = 0; i < 512; i++)
        printf("%d. Bruler : %d\n", i, Bruler[i]);
        */

    for(addr = 0; addr < size * 2; addr++)
    {
        uint64_t addr_ = (uint64_t)(addr<<12);
        int bank = get_bank(addr_);
        int row = get_row(addr_);
        Bruler[addr] = bank;
        Rruler[addr] = row;
        printf("%d. bank: %d, row: %d\n", addr, bank, row);
    }
}

int detect_cont_pages(int size, int interval)
{
    /*
    int offset, bank, cont_offset = -1;
    for(offset = 0; offset < size; offset++)
    {
        printf("offset : %d\n", offset);

        for(bank = 0; bank < 16; bank++)
        {
            cont_offset = __detect_cont_pages(offset, interval, bank);
            if(cont_offset != -1)
            {
                printf("bank : %d\n", bank);
                printf("ab : %d, cd : %d, ef : %d, gh : %d\n", ab, cd, ef, gh);
                return cont_offset + offset;
            }
        }
    }
    */
    int offset, bank, row, cont_offset = -1;
    for(offset = 0; offset < size - 1024; offset++)
    {
        for(bank = 0; bank < 16; bank++)
        {
            cont_offset = __detect_cont_pages(offset, interval, bank, row);
            if(cont_offset != -1)
            {
                printf("bank: %d, row: %d\n", bank, row);
                printf("ab: %d, cd: %d, ef: %d, gh: %d\n", ab, cd, ef, gh);
                return cont_offset + offset;
            }
        }
    }
    return -1;
}
int __detect_cont_pages(int Loffset, int interval, int bank)
{
    int Roffset, diff = 0;
    for(Roffset = 0; Roffset < 512 - interval; Roffset++)
    {
        diff = diff_ruler(Roffset, Loffset, interval, bank);
//        printf("%d. diff : %d\n", Roffset, diff);
//        if(diff <= 4)
        if(diff == 0)
            return Roffset;
    }
    return -1;
}

int diff_ruler(int Roffset, int Loffset, int interval, int bank)
{

    int i, LBdiff = 0, BLdiff = 0;

    for(i = 0; i < interval; i++)
        if(PT[3][Loffset + i] == 1)
            if(Bruler[Roffset + i] != bank)
                LBdiff++;
    for(i = 0; i < interval; i++)
        if(Bruler[Roffset + i] == bank)
            if(PT[3][Loffset + i] != 1)
                BLdiff++;

    ab = bank;
    ef = Roffset;
    gh = Loffset;


//    return max(RBdiff, RPdiff);
    int max = ((LBdiff > BLdiff) ? LBdiff : BLdiff);
    return max;
}

int diff_ruler_(int Roffset, int Loffset, int interval, int bank)
{

    int i, LBdiff = 0, BLdiff = 0;

    printf("Latency: %"PRIu64", Ruler: %d\n", PT[3][Loffset], Bruler[Roffset]);

    for(i = 0; i < interval; i++)
        if(PT[3][Loffset + i] == 1)
            if(Bruler[Roffset + i] != bank)
                LBdiff++;
    for(i = 0; i < interval; i++)
        if(Bruler[Roffset + i] == bank)
            if(PT[3][Loffset + i] != 1)
                BLdiff++;

    for(i = 0; i < interval; i++)
        printf("ruler : %d, latency : %"PRIu64"\n", Bruler[i + Roffset], PT[3][Loffset + i]);


    int max = ((LBdiff > BLdiff) ? LBdiff : BLdiff);
    return max;
}


void check_array()
{
    int size = 262144*4;
    int a[size];
    int i;
    uint64_t start, end;
    for(i = 0; i < size/1024; i++)
    {
        start = time_stamp();
        a[i*1024] = 1;
        end = time_stamp();
        printf("%d. PFN : %"PRIx64", time : %"PRIx64"\n", i, convert_VA_into_PFN((uint64_t)&a[i*1024]), end-start);

    }
}

int check_latency(uint64_t VA1, uint64_t VA2)
{
    int i;
    uint64_t start, end;
    uint64_t latency = 0;
    char *a = (char *)VA1;
    char *b = (char *)VA2;
    for(i = 0; i < 100; i++)
    {
        start = time_stamp();
        __asm__ __volatile__
            (
             "clflush (%0);"
             "clflush (%1);"
             "mov (%0), %%r8;"
             "mov (%1), %%r9;"
             "mfence;"
             :: "q" (a), "p" (b)
            );
        end = time_stamp();
        latency += end - start;
    }
    return latency / 100;
}
int single_check_latency(uint64_t VA)
{
    int i;
    uint64_t start, end;
    uint64_t latency = 0;
    char *a = (char *)VA;
//    for(i = 0; i < 1000; i++)
//    {
        start = time_stamp();
        __asm__ __volatile__
            (
             "clflush (%0);"
             "mov (%0), %%r8;"
//             "clflush (%0);"
//             "mfence;"
             :: "q" (a)
            );
        end = time_stamp();
        latency += end - start;
//    }
//    return latency / 1000;
        
        return latency;
}

unsigned long time_stamp()
{
    unsigned long low, high;
    __asm__ __volatile__(
            "mfence\n\t"
            "rdtscp\n\t"
            "mfence\n\t"
            : "=a" (low), "=d" (high)
            );
    return ((low) | high << 32);
}

int get_bank(uint64_t src)
{
    uint64_t a = 0, ret = 0;
    int i;
    for(i = 0; i < BANK_NUM/2; i++)
    {
        a = (src & BANK_BIT[i]) ^ BANK_BIT[i];
        if((a != BANK_BIT[i])&&(a != 0))
            ret = (ret | (1 << i));
    }
    return ret;
}
int get_row(uint64_t src)
{
    int ret = ((src & ROW_MASK) >> ROW_START);
    return ret;
}

int __check_interval(int interval, int index, int offset)
{
    int i;
    if(get_bank(PT[0][index]) == get_bank(PT[0][index + interval + offset]))
        return 1;
    else
        return 0;

}
int check_interval(int interval)
{
    int i, j;
    int max = SIZE/4096;
    int diff_row, diff_bank;
    int count = 0, not_count = 0;
    int check = 0;


}
void print_PFN()
{
    int i;
    for(i = 0; i < SIZE/4096; i++)
    {
        printf("PFN: %"PRIx64"\n", PT[0][i]);
    }

}
void init_PT(char *addr, uint64_t size)
{
    int i;
    char *ptr = addr;
    for(i = 0; i < size/4096; i++)
    {
        memset(ptr, 1, 1);
        PT[0][i] = convert_VA_into_PFN((uint64_t)ptr);
        PT[1][i] = (uint64_t)ptr;

        ptr = ptr + 4096;
    }
    return ;
}

int print_contiguity(int degree, int size)
{
    
    int max = SIZE/4096;
    int i;
    int cont = 1;
    int result = 0;
    int max_cont = 0;
    int max_PFN_index = 0;
    for(i = 0; i < max - 1; i++)
    {
        if(PT[0][i+1]-1 == (PT[0][i]))
            cont++;
        else
        {
            if(cont == 1)
                ;
            else
            {
                printf("%d. cont : %d\n", i-cont+1, cont);
                if(max_cont < cont)
                {
                    max_cont = cont;
                    max_PFN_index = i - max_cont + 1;
                }
            }
            cont = 1;
        }
    }
    /*
    if (max_cont >= degree)
        return max_PFN_index;
    else
        return 0;
        */
        
//    int i;
    for(i = 0; i < size; i++)
    {
        printf("%d. PA: %"PRIx64", cont: %"PRIu64", latency : %"PRIu64", ", i, PT[0][i], PT[2][i], PT[3][i]);
        printf("BANK1: %d, BANK2: %d, ", get_bank((uint64_t)(PT[0][0]<<12)), get_bank((uint64_t)(PT[0][i]<<12)));
        printf("ROW1: %d, ROW2: %d\n", get_row((uint64_t)(PT[0][0]<<12)), get_row((uint64_t)(PT[0][i]<<12)));
    }

    return 0;

}
uint64_t convert_VA_into_PFN(uint64_t virt_addr)
{
    uint64_t value;
    int got = pread(pagemap, &value, 8, ((uintptr_t)(virt_addr) / 0x1000) * 8);
    assert(got == 8);
    uint64_t page_frame_number = value&((1ULL << 54) - 1);

    return page_frame_number;
}
