#include "mapping.h"

int main()
{
    pagemap = open("/proc/self/pagemap", O_RDONLY);
    char *addr = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    printf("ptr : %"PRIx64"\n", convert_VA_into_PFN((uint64_t)addr));
}
