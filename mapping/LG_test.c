#include "mapping.h"

int main()
{
    int i, max_row = 0, temp_row;
    char *temp;

    while(1)
    {
        if(fork() == 0)
        {
            pagemap = open("/proc/self/pagemap", O_RDONLY);
            char *addr = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

            init_real_pt(addr, SIZE);

            for(i = 0; i< 10000;i++)
            {
                //        printf("%d. pa : %"PRIx64", va : %"PRIx64"\n", i, pt[0][i], pt[1][i]);

            }

            int row1 = get_row(convert_VA_into_PFN((uint64_t)addr) << 12);
            int bank1 = get_bank(convert_VA_into_PFN((uint64_t)addr) << 12);


            for(temp = addr; temp < addr + SIZE; temp += 4096)
            {
                temp_row = get_row((uint64_t)addr);
                if(max_row < temp_row)
                    max_row = temp_row;
            }


            char *addr2;
            for(i = 0; i < row1 - 600; i++){
                addr2 = get_va(i, bank1, addr, SIZE);
            }

            if(addr2 == 0){
                for(i = row1 + 600; i < max_row; i++){
                    addr2 = get_va(i, bank1, addr, SIZE);
                }
            }

            char *addr1;
            int start = (row1 / 512) * 512 + 1;
            int end = (row1/ 512 + 1) * 512;


            for(i = start; i < end; i++)
                addr1 = get_va(i, bank1, addr, SIZE);


            uint64_t pa1 = convert_VA_into_PFN((uint64_t)addr) << 12;
            uint64_t pa2 = convert_VA_into_PFN((uint64_t)addr2) << 12;
            uint64_t pa3 = convert_VA_into_PFN((uint64_t)addr1) << 12;

            if(!(get_bank(pa2) == 0 || get_row(pa2) == 0 || get_bank(pa3) == 0 || get_row(pa3) == 0)){
                printf("bank: %d, row: %d ----------- bank2: %d, row2: %d ------------ bank1: %d, row1: %d\n",
                        get_bank(pa1), get_row(pa1),
                        get_bank(pa2), get_row(pa2),
                        get_bank(pa3), get_row(pa3)
                      );


                printf("1. latency : %d\n", check_latency((uint64_t)addr, (uint64_t)addr2));
                printf("2. latency : %d\n", check_latency((uint64_t)addr, (uint64_t)addr1));
            }
            munmap(addr, SIZE);
        }
        else
            wait(0);
    }



}
