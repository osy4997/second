#include <signal.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/sysinfo.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <limits.h>

//#define SIZE 1024*1024*1024ULL
#define SIZE 1024*1024*2ULL
#define BANK_MASK 0b111111110000000000000
#define ROW_MASK 0b1111111111111100000000000000000
#define MIRROR 0b11111100000000000000000000
#define ROW_START 17
#define BANK_NUM 16
#define ROW_NUM SIZE/(BANK_NUM * 4096 * 2)
#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b;})

#define MALLOC mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
#define HPMALLOC mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE | MAP_HUGETLB, -1, 0);
#define PAGEMAP open("/proc/self/pagemap", O_RDONLY);

#define data 256
#define gran 4096
#define Zero 0 
#define RowStripe 1
#define _RowStripe 2
#define AllCheck 0
#define PartCheck 1
//unsigned long high, low;
//uint64_t BANK_BIT[4] = {(1<<13) + (1<<17), (1<<14) + (1<<18), (1<<15) + (1<<19), (1<<16) + (1<<20)};
uint64_t BANK_BIT[4] = {(1<<13) + (1<<17), (1<<14) + (1<<18), (1<<15) + (1<<19), (1<<16) + (1<<20)};
int latency_regularity[16][8] = {0};

char *p[16][16] = {0};

int ab, cd, ef, gh;

unsigned int bitflip = 0;
#define TIMESTAMP(high, low)                        \
    __asm__ __volatile__(                           \
            "mfence;"                               \
            "rdtscp;"                               \
            "mfence;"                               \
            : "=a" (low), "=d" (high)               \
            );                                      \
/////////////////////////////////////////////////////
int pagemap;
//uint64_t PT[4][262144] = {0}; // 0 - PA, 1 - VA, 2 - cont, 3 - latency
uint64_t PT[4][16384] = {0};
uint64_t pt[2][262144] = {0};
//char *DRAM[8][16384][2] = {0};
char *DRAM[16][8192][2] = {0};
//int off[8][16384] = {0};
int off[16][8192] = {0};
//uint64_t PFN_check[262144] = {0};
int Bruler[1024] = {0};
int Rruler[1024] = {0};
int detect_contiguous_pages(int, int);
uint64_t convert_VA_into_PFN(uint64_t);
int print_contiguity(int, int);
void init_PT(char *, uint64_t);
void print_PFN(void);
int check_interval(int);
int get_bank(uint64_t);
int get_row(uint64_t);
unsigned long time_stamp();
unsigned long check_latency(char *VA1, char *VA2);
unsigned long single_check_latency(char *VA);
void check_array();
int init_cont(int, int);
void init_real_pt(volatile char *addr, int size);
char *get_va(int row, int bank, char *addr, int size);
char *get_va_from_bank(int bank, int index);
void print_real_pt();
void make_DRAM(char *ptr);
char OddPattern, EvenPattern;

void set_pattern(int pattern)
{
    int i, j, k;
    switch(pattern)
    {
        case Zero:
            OddPattern = 0x0;
            EvenPattern = 0x0;
            break;
        case RowStripe:
            OddPattern = 0x0;
            EvenPattern = 0xff;
            break;
        case _RowStripe:
            OddPattern = 0xff;
            EvenPattern = 0x0;
            break;
    }
    for(i = 0; i < BANK_NUM; i++)
    {

        for(j = 0; j < 8192; j++)
        {
            if(DRAM[i][j][0] != 0)
            {
                for(k = 0; k < 8192; k++)
                {
                    if(j % 2 == 0)
                    {
                        *(DRAM[i][j][0] + k) = EvenPattern;
                    }
                    else
                    {
                        *(DRAM[i][j][0] + k) = OddPattern;
                    }
                }
            }
        }
    }

}
void check_pattern(int opt, int bank, int row_start, int row_end)
{
    int i, j, k;
    if(opt == AllCheck)
    {
        for(i = 0; i < BANK_NUM; i++)
        {
            for(j = 0; j < 8192; j++)
            {
                if(DRAM[i][j][0] != 0)
                {
                    for(k = 0; k < 8192; k++)
                    {
                        if(j % 2 == 0)
                        {
                            if(*(DRAM[i][j][0] + k) != EvenPattern)
                            {
                                unsigned long pfn = convert_VA_into_PFN((uint64_t)(DRAM[i][j][0]+k));
                               printf("error! bank: %d, row: %d, ptr: %p, pfn: %lu\n", i, j, DRAM[i][j][0] + k, pfn);
                        *(DRAM[i][j][0] + k) = EvenPattern;
                        
                                bitflip++;
                            }
                        }
                        else
                        {
                            if(*(DRAM[i][j][0] + k) != OddPattern)
                            {
                                unsigned long pfn = convert_VA_into_PFN((uint64_t)(DRAM[i][j][0]+k));
                                printf("error! bank: %d, row: %d, ptr: %p, pfn: %lu\n", i, j, DRAM[i][j][0] + k, pfn);

                        *(DRAM[i][j][0] + k) = OddPattern;
                                bitflip++;
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        for(j = row_start; j < row_end; j++)
        {
            for(k = 0; k < 8192; k++)
            {
                if(j % 2 == 0)
                {
                    if(*(DRAM[bank][j][0]+k) != EvenPattern )
                    {
                        printf("error! bank: %d, row: %d, ptr: %p\n", i, j, DRAM[i][j][0] + k);
                        *(DRAM[i][j][0] + k) = EvenPattern;
                    }
                }
                else
                {
                    if(*(DRAM[bank][j][0]+k) != OddPattern)
                    {

                        printf("error! bank: %d, row: %d, ptr: %p\n", i, j, DRAM[i][j][0] + k);
                        *(DRAM[i][j][0] + k) = OddPattern;
                    }
                }
            }
        }
    }
}
void make_DRAM(char *ptr)
{
    char *iter;
    uint64_t pa;
    int bank, row;
    for(iter = ptr; iter < ptr + SIZE; iter += 4096)
    {
        pa = convert_VA_into_PFN((uint64_t)iter) << 12;
        bank = get_bank(pa);
//                row = get_row(pa);
        row = get_row_v2(pa);
        DRAM[bank][row][off[bank][row]++] = iter;
        //        printf("ptr : %p, bank : %d, row : %d\n", iter, get_bank(pa), get_row(pa));
    }

}


char *get_bank_addr(char *addr, int bank)
{
    int i;
    char *ptr = addr;
    for(i = 0; i < SIZE/4096; i++)
    {
        memset(ptr, 1, 1); 
        uint64_t pfn = convert_VA_into_PFN((uint64_t)ptr);
        uint64_t pa = pfn << 12; 
        if(get_bank(pa) == bank)
            return ptr;
        ptr = ptr + 4096;
    }
    return 0;
}

char *Get_Addr_Latency(char *base, char *addr)
{
    int i, threshold = 550, j = 0;
    unsigned long latency;
    char *temp;
    for(i = 0; i < SIZE/4096; i++)
    {
        temp = base + i * 4096;
        latency = check_latency(addr, temp);
        if(latency > threshold)
            //        if(i == 137 || i == 272 || i == 409 || i == 544)
            p[0][j++] = temp;
        //            printf("%d. %p, %"PRIx64", %lu\n", i+1, temp, convert_VA_into_PFN((uint64_t)temp), latency);
        if(j == 4)
            return 0;
    }
    return 0;

}
void print_real_pt()
{
    int i;
    for(i = 0; i < SIZE/4096; i++)
        printf("pa: %"PRIx64", va: %"PRIx64", bank: %d, row: %d\n", pt[0][i], pt[1][i], get_bank(pt[0][i]), get_row(pt[0][i]));
}
char *get_va_from_bank(int bank, int row)
{
    int i;
    for(i = 0; i < SIZE/4096; i++)
    {
        if((get_bank(pt[0][i]) == bank) && (get_row(pt[0][i]) != row))
        {
            return (char *)pt[1][i];
        }
    }
    return 0;
}


char *get_va(int row, int bank, char *addr, int size)
{
    int i;
    char *temp;
    for(i = 0; i < SIZE / 4096; i++)
    {
        uint64_t pa = pt[0][i];
        if(get_row(pa) == row && get_bank(pa) == bank)
            return (char *)pt[1][i];
    }

    return 0;
}

void init_real_pt(volatile char *addr, int size)
{
    volatile char *temp;
    int i = 0;
    for(temp = addr; temp < addr + size; temp += 4096){
        *temp = 1;
        pt[0][i] = convert_VA_into_PFN((uint64_t)temp) << 12;

        pt[1][i] = (uint64_t)temp;

        i++;
    }
    //    printf("IN INIT - uint64_t va : %"PRIx64", va : %p\n", (uint64_t)addr/*PT[1][0]*/, addr);
}

int init_cont(int size, int threshold)
{
    int i, j, cont = 1, num = 0;
    for(i = 1; i < size; i++)
    {
        if((PT[0][i-1] + 1)  == PT[0][i])
            cont++;
        else
        {
            if(cont >= threshold)
                for(j = 0; j <= cont; j++)
                {
                    PT[2][i - j/* - 1*/] = 1;
                }
            num++;
            cont = 1;
        }
    }

    for(i = 0; i < size; i++)
    {
        if(check_latency((char *)PT[1][0], (char *)PT[1][i]) > 700)
            PT[3][i] = 1;
        else
            PT[3][i] = 0;
        //        PT[3][i] = check_latency(PT[1][0], PT[1][i]);
    }
    return num;
}
void make_ruler() // depends on # of BANK
{
    int size = 1 << BANK_NUM;
    int bank_size = 1 << (BANK_NUM/2);
    int Btemp[16][32] = {0};
    int Rtemp[16][32] = {0};
    int addr;
    int index[16] = {0};

    /*
       for(addr = 0; addr < size * 2; addr++)
       {
       uint64_t addr_ = (uint64_t)(addr<<12);
       int bank = get_bank((uint64_t)(addr_));
       int row = get_row((uint64_t)(addr_));
       Btemp[bank][index[bank]] = addr;
       index[bank]++;
       }


       int i, j, k;


       for(k = 0; k < 2; k++)
       {
       for(i = 0; i < 16; i++)
       {
       for(j = 0; j < 32; j++)
       {
       Bruler[Btemp[i][j] + 512 * k] = i;
       }
       }
       }

       for(i = 0; i < 512; i++)
       printf("%d. Bruler : %d\n", i, Bruler[i]);
     */

    for(addr = 0; addr < size * 2; addr++)
    {
        uint64_t addr_ = (uint64_t)(addr<<12);
        int bank = get_bank(addr_);
        int row = get_row(addr_);
        Bruler[addr] = bank;
        Rruler[addr] = row;
        printf("%d. bank: %d, row: %d\n", addr, bank, row);
    }
}

int detect_cont_pages(int size, int interval)
{
    /*
       int offset, bank, cont_offset = -1;
       for(offset = 0; offset < size; offset++)
       {
       printf("offset : %d\n", offset);

       for(bank = 0; bank < 16; bank++)
       {
       cont_offset = __detect_cont_pages(offset, interval, bank);
       if(cont_offset != -1)
       {
       printf("bank : %d\n", bank);
       printf("ab : %d, cd : %d, ef : %d, gh : %d\n", ab, cd, ef, gh);
       return cont_offset + offset;
       }
       }
       }
     */
    int offset, bank, row, cont_offset = -1;
    for(offset = 0; offset < size - 1024; offset++)
    {
        for(bank = 0; bank < 16; bank++)
        {
            cont_offset = __detect_cont_pages(offset, interval, bank, row);
            if(cont_offset != -1)
            {
                printf("bank: %d, row: %d\n", bank, row);
                printf("ab: %d, cd: %d, ef: %d, gh: %d\n", ab, cd, ef, gh);
                return cont_offset + offset;
            }
        }
    }
    return -1;
}
int __detect_cont_pages(int Loffset, int interval, int bank)
{
    int Roffset, diff = 0;
    for(Roffset = 0; Roffset < 512 - interval; Roffset++)
    {
        diff = diff_ruler(Roffset, Loffset, interval, bank);
        //        printf("%d. diff : %d\n", Roffset, diff);
        //        if(diff <= 4)
        if(diff == 0)
            return Roffset;
    }
    return -1;
}

int diff_ruler(int Roffset, int Loffset, int interval, int bank)
{

    int i, LBdiff = 0, BLdiff = 0;

    for(i = 0; i < interval; i++)
        if(PT[3][Loffset + i] == 1)
            if(Bruler[Roffset + i] != bank)
                LBdiff++;
    for(i = 0; i < interval; i++)
        if(Bruler[Roffset + i] == bank)
            if(PT[3][Loffset + i] != 1)
                BLdiff++;

    ab = bank;
    ef = Roffset;
    gh = Loffset;


    //    return max(RBdiff, RPdiff);
    int max = ((LBdiff > BLdiff) ? LBdiff : BLdiff);
    return max;
}

int diff_ruler_(int Roffset, int Loffset, int interval, int bank)
{

    int i, LBdiff = 0, BLdiff = 0;

    printf("Latency: %"PRIu64", Ruler: %d\n", PT[3][Loffset], Bruler[Roffset]);

    for(i = 0; i < interval; i++)
        if(PT[3][Loffset + i] == 1)
            if(Bruler[Roffset + i] != bank)
                LBdiff++;
    for(i = 0; i < interval; i++)
        if(Bruler[Roffset + i] == bank)
            if(PT[3][Loffset + i] != 1)
                BLdiff++;

    for(i = 0; i < interval; i++)
        printf("ruler : %d, latency : %"PRIu64"\n", Bruler[i + Roffset], PT[3][Loffset + i]);


    int max = ((LBdiff > BLdiff) ? LBdiff : BLdiff);
    return max;
}


void check_array()
{
    int size = 262144*4;
    int a[size];
    int i;
    uint64_t start, end;
    for(i = 0; i < size/1024; i++)
    {
        start = time_stamp();
        a[i*1024] = 1;
        end = time_stamp();
        printf("%d. PFN : %"PRIx64", time : %"PRIx64"\n", i, convert_VA_into_PFN((uint64_t)&a[i*1024]), end-start);

    }
}
unsigned long check_latency_v2(char *va1, char *va2)
{
    int i, j, ab, total = 0;
    unsigned long start_high, start_low, end_high, end_low, latency = 0, temp;
    for(i = 0; i < 1000; i++)
    {
        __asm__ __volatile__
            (
             "mfence;"
             "clflush (%0);"
             "mfence;"
             "clflush (%1);"
             "mfence;"
             "movq (%0), %%r8;"
             "mfence;"
             :: "q" (va1), "p" (va2)
            );
        __asm__ __volatile__
            (
             "addq $1, %r8;"
             "mfence;"
             "addq $1, %r8;"
             "mfence;"
             "addq $1, %r8;"
             "mfence;"
             "addq $1, %r8;"
             "mfence;"
             "addq $1, %r8;"
             "mfence;"
             "addq $1, %r8;"
             "mfence;"
            );
        TIMESTAMP(start_high, start_low);
        __asm__ __volatile__
            (
             "movq (%1), %%r9;"

             :: "q" (va1), "p" (va2)
            );
        TIMESTAMP(end_high, end_low);

        temp = ((end_high << 32) + end_low) - ((start_high << 32) + start_low);
        if(temp < 1000){
            latency += temp;
            total++;
        }
    }
    return latency / total;
}
unsigned long check_latency(char *va1, char *va2)
{
    int i, total = 0;
    unsigned long start_high, start_low, end_high, end_low, latency = 0, temp;
    for(i = 0; i < 1000; i++)
    {
        __asm__ __volatile__
            (
             "mfence;"
             "clflush (%0);"
             "mfence;"
             "clflush (%1);"
             :: "q" (va1), "p" (va2)
            );

        TIMESTAMP(start_high, start_low);
        __asm__ __volatile__
            (
             "mfence;"
             "movq (%0), %%r8;"
             "mfence;"
             "movq (%1), %%r9;"
             "mfence;"
             :: "q" (va1), "p" (va2)
            );
        TIMESTAMP(end_high, end_low);

        temp = ((end_high << 32) + end_low) - ((start_high << 32) + start_low);
        if(temp < 1000){
            latency += temp;
            total++;
        }

    }
    return latency / total;
}
void double_sided_hammer(char *va1, char *va2)
{
    int i;
    for(i = 0; i < 5000000; i++)
    {
        __asm__ __volatile__
            (
             "clflush (%0);"
             "clflush (%1);"
             "movq (%0), %%r8;"
             "movq (%1), %%r9;"
             :: "q" (va1), "p" (va2)
            );
    }
}
unsigned long latency_without_clflush(char *va)
{
    int i;
    unsigned long start_high, start_low, end_high, end_low;
    uint64_t latency = 0;
    TIMESTAMP(start_high, start_low);
    __asm__ __volatile__
        (
         "movq (%0), %%r8;"
         :: "p" (va)
        );
    TIMESTAMP(end_high, end_low);

    //    __asm__ __volatile__("clflush (%0);":: "p" (va));

    latency = ((end_high << 32) + end_low) - ((start_high << 32) + start_low);
    return latency;
}
unsigned long write_after_read(char *va)
{
    int i;
    unsigned long start_high, start_low, end_high, end_low;
    __asm__ __volatile__("clflush (%0);":: "q" (va));
    TIMESTAMP(start_high, start_low);
    __asm__ __volatile__
        (
         "movq $1, (%0);"
         "movq (%0), %%r8;"
         :: "p"(va)
        );
    TIMESTAMP(end_high, end_low);
    return ((end_high << 32) + end_low) - ((start_high << 32) + start_low);

}
unsigned long single_check_latency(char *va)
{
    int i;
    unsigned long start_high, start_low, end_high, end_low;
    uint64_t latency = 0;
    __asm__ __volatile__("mfence;" "clflush (%0);":: "q" (va));
    TIMESTAMP(start_high, start_low);
    __asm__ __volatile__
        (
         "mfence;"
         //         "movq $1, (%0);"
         "mov (%0), %%r8;"
         "mfence;"
         :: "p" (va)
        );
    TIMESTAMP(end_high, end_low);

    //    __asm__ __volatile__("clflush (%0);":: "q" (va));
    return ((end_high << 32) + end_low) - ((start_high << 32) + start_low);
}

unsigned long write_latency(char *va)
{
    int i;
    unsigned long start_high, start_low, end_high, end_low;
    uint64_t latency = 0;
    __asm__ __volatile__("clflush (%0);":: "q" (va));
    TIMESTAMP(start_high, start_low);
    __asm__ __volatile__
        (
         "mfence;"
         "movq $1, (%0);"
         :: "p" (va)
        );
    TIMESTAMP(end_high, end_low);
    return ((end_high << 32) + end_low) - ((start_high << 32) + start_low);
}

unsigned long time_stamp()
{
    unsigned long low, high;
    __asm__ __volatile__(
            "mfence\n\t"
            "rdtscp\n\t"
            "mfence\n\t"
            : "=a" (low), "=d" (high)
            );
    return ((low) | high << 32);
}

int get_bank(uint64_t src)
{
    uint64_t a = 0, ret = 0;
    int i;
    for(i = 0; i < 4; i++)
    {
        a = (src & BANK_BIT[i]) ^ BANK_BIT[i];
        if((a != BANK_BIT[i])&&(a != 0))
            ret = (ret | (1 << i));
    }
    return ret;
}
uint64_t bit_swap(uint64_t src, unsigned int a, unsigned int b)
{
    uint64_t bit1 = (src >> a) & 1;
    uint64_t bit2 = (src >> b) & 1;
    unsigned int x = bit1 ^ bit2;
    x = (x << a) | (x << b);
    return src ^ x;
}
uint64_t scrambling(uint64_t src)
{
    uint64_t temp = (src & 8) >> 3;
    return src ^ (temp << 1) ^ (temp << 2);
}
int get_row(uint64_t src)
{
    int ret = ((src & ROW_MASK) >> ROW_START);
    return ret;
}
int get_row_v2(uint64_t src) // for SAMSUNG DRAM
{
    int bank = get_bank(src);
    int ret = ((src & ROW_MASK) >> ROW_START);
    if((bank & 4) != 0) // mirroring
    {
        ret = bit_swap(ret, 3, 4);
        ret = bit_swap(ret, 5, 6);
        ret = bit_swap(ret, 7, 8);
    }
//    return scrambling(ret);
    return ret;
}

int __check_interval(int interval, int index, int offset)
{
    int i;
    if(get_bank(PT[0][index]) == get_bank(PT[0][index + interval + offset]))
        return 1;
    else
        return 0;

}

static unsigned long get_row_plus(unsigned long phy, int inc){

    unsigned long pa = phy << 12;
    unsigned long bank_old = get_bank(pa);
    unsigned long row_new = get_row_v2(pa) + inc;

    if((bank_old & 4) == 0){

        row_new = bit_swap(row_new, 3, 4);
        row_new = bit_swap(row_new, 5, 6);
        row_new = bit_swap(row_new, 7, 8);
    }

    unsigned long temp = row_new << ROW_START;
    temp = (pa & (~ROW_MASK)) | temp;
    unsigned long bank_new = get_bank(temp);
    if(bank_new != bank_old)
    {
        unsigned long temp1 = pa ^ temp;
        int i;
        for(i = 0; i < 4; i++)
        {
            unsigned long temp2 = temp1 & BANK_BIT[i];
            if(temp2)
            {
                temp2 ^= BANK_BIT[i];
                temp ^= temp2;
            }
        }
    }
    printf("bank: %d\n", get_bank(temp));
    return temp >> 12;
}
static unsigned long get_row_minus(unsigned long phy, int inc){

    unsigned long pa = phy << 12;
    unsigned long bank_old = get_bank(pa);
    unsigned long row_new = get_row_v2(pa) - inc;

    if((bank_old & 4) == 0){

        row_new = bit_swap(row_new, 3, 4);
        row_new = bit_swap(row_new, 5, 6);
        row_new = bit_swap(row_new, 7, 8);
    }

    unsigned long temp = row_new << ROW_START;
    temp = (pa & (~ROW_MASK)) | temp;
    unsigned long bank_new = get_bank(temp);
    if(bank_new != bank_old)
    {
        unsigned long temp1 = pa ^ temp;
        int i;
        for(i = 0; i < 4; i++)
        {
            unsigned long temp2 = temp1 & BANK_BIT[i];
            if(temp2)
            {
                temp2 ^= BANK_BIT[i];
                temp ^= temp2;
            }
        }
    }
    return temp >> 12;
}
int check_interval(int interval)
{
    int i, j;
    int max = SIZE/4096;
    int diff_row, diff_bank;
    int count = 0, not_count = 0;
    int check = 0;


}
void print_PFN()
{
    int i;
    for(i = 0; i < SIZE/4096; i++)
    {
        printf("PFN: %"PRIx64"\n", PT[0][i]);
    }

}
void init_PT(char *addr, uint64_t size)
{
    int i;
    char *ptr = addr;
    for(i = 0; i < size/4096; i++)
    {
        memset(ptr, 1, 1);
        PT[0][i] = convert_VA_into_PFN((uint64_t)ptr);
        PT[1][i] = (uint64_t)ptr;

        ptr = ptr + 4096;
    }
    return ;
}

int print_contiguity(int degree, int size)
{

    int max = SIZE/4096;
    int i;
    int cont = 1;
    int result = 0;
    int max_cont = 0;
    int max_PFN_index = 0;
    for(i = 0; i < max - 1; i++)
    {
        if(PT[0][i+1]-1 == (PT[0][i]))
            cont++;
        else
        {
            if(cont == 1)
                ;
            else
            {
                printf("%d. cont : %d\n", i-cont+1, cont);
                if(max_cont < cont)
                {
                    max_cont = cont;
                    max_PFN_index = i - max_cont + 1;
                }
            }
            cont = 1;
        }
    }
    /*
       if (max_cont >= degree)
       return max_PFN_index;
       else
       return 0;
     */

    //    int i;
    for(i = 0; i < size; i++)
    {
        printf("%d. PA: %"PRIx64", cont: %"PRIu64", latency : %"PRIu64", ", i, PT[0][i], PT[2][i], PT[3][i]);
        printf("BANK1: %d, BANK2: %d, ", get_bank((uint64_t)(PT[0][0]<<12)), get_bank((uint64_t)(PT[0][i]<<12)));
        printf("ROW1: %d, ROW2: %d\n", get_row((uint64_t)(PT[0][0]<<12)), get_row((uint64_t)(PT[0][i]<<12)));
    }

    return 0;

}
uint64_t convert_VA_into_PFN(uint64_t virt_addr)
{
    uint64_t value;
    int got = pread(pagemap, &value, 8, ((uintptr_t)(virt_addr) / 0x1000) * 8);
    assert(got == 8);
    uint64_t page_frame_number = value&((1ULL << 54) - 1);

    return page_frame_number;
}
