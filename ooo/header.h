#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#include <stdlib.h>
#include <inttypes.h>

#define data 256 
#define gran 4096

unsigned long time_stamp()
{
    unsigned long low, high;

    __asm__ __volatile__(
            "mfence;"
            "rdtscp;"
            "mfence;"
            : "=a" (low), "=d" (high)
            );  
    return (low | high << 32);
}

unsigned long check_latency(char* va) 
{
    int i;
    unsigned long start, end;
    start = time_stamp();
    __asm__ __volatile__
        (  
//         "mfence;"
//         "clflush (%0);"
         "mov (%0), %%r8;"
//         "clflush (%0);"
//         "mfence;"
         :: "q" (va)
        );  
    end = time_stamp();
    return end - start;
}
void cache_flush(char* va) 
{
    __asm__ __volatile__ ("clflush (%0);" :: "q" (va));
    return ;
}

