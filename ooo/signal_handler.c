#include "../header.h"
char probe_array[data * gran];

void access()
{
    int i;
    char a;
    for(i = 0; i < data; i++)
        a = probe_array[i * gran];
//        probe_array[i * gran] = 1;
}

void cache_flush(char* va)
{
    __asm__ __volatile__(
            "clflush (%0);"
            :: "q" (va)
            );

}
void sigint_handler(int sig)
{
    int i;
    unsigned long start, end;
    unsigned long latency;
    printf("Caught Segmentation Fault!\n");
    pagemap = PAGEMAP;
    printf("addr: %p, PFN: %"PRIx64"\n", &probe_array[0], convert_VA_into_PFN((uint64_t)&probe_array[0]));
    
    for(i = 0; i < data; i++)
    {
//        start = time_stamp();
//        probe_array[i * gran] = 1;
//        end = time_stamp();
//        latency = end - start;
//        cache_flush(&probe_array[i * gran]);
//        latency = single_check_latency(&probe_array[i * gran]);
        latency = latency_without_clflush(&probe_array[i * gran]);
        printf("%d. latency: %lu\n", i, latency);

        if(latency < 400)
        {
            printf("find it!\n");
//            exit(0);
        }
    }

//    sleep(1);
//    return ;
    int a;
    for(i = 0; i < INT_MAX/10; i++)
        a = i;
    exit(0);
}

int main()
{
    if(signal(SIGSEGV, sigint_handler) == SIG_ERR)
        printf("signal error");

    char a;
    int *p = 0;
    int i;
//    while(1){
    for(i = 0; i < 4; i++){
        if(fork() == 0){
            printf("pid : %d\n", getpid());
            access();
            int i;
            for(i = 0; i < data; i++)
            {
                cache_flush(&probe_array[i * gran]);
            }
            *p = 1;
            access();
        }
        else
            wait(0);
    }
    return 0;
}
