#include "header.h"

static char probe_array[gran * data];
void access_latency()
{
    int i;
    for(i = 0; i < data; i++)
        printf("%d. latency : %lu\n", i, check_latency(&probe_array[gran * i]));
}
int main()
{
    int i;
    unsigned long latency;

    access_latency();
//    sleep(1);
//    usleep(1);
//    nanosleep(100000);
    for(i = 0; i < UINT_MAX/1000; i++)
        latency = 0;
    printf("after sleep ...\n");
//    access_latency();
//    printf("after immediately ...\n");


    int pid = fork();
    if(pid == 0)
    {
        printf("child ! \n");
        access_latency();
    }
    else
    {
        wait(0);
        printf("parent !\n");
        access_latency();
    }
    /*

    sleep(3);
    printf("after sleep 3 secs ...\n");

    if(pid == 0)
    {
        printf("second child!\n");
        access_latency();
    }
*/
}
