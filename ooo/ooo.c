#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

#define data 8//256
char probe_array[data * 4096];

unsigned long time_stamp()
{
    unsigned long low, high;
    __asm__ __volatile__(
            "mfence\n\t"
            "rdtscp\n\t"
            "mfence\n\t"
            : "=a" (low), "=d" (high)
            );
    return (low | high << 32);
}

void access()
{
    int i;
    char c;

    for(i = 0; i < data; i++)
    {
        c = probe_array[i * 4096];
    }
    return ;
}
int check_latency( char* va)
{
    int i;;
    unsigned long start, end;
    unsigned long latency = 0;
    char *a = (char *)va;
    start = time_stamp();
    __asm__ __volatile__
        (
         "clflush (%0);"
         "mov (%0), %%r8;"
         :: "q" (a)
        );
    end = time_stamp();
    latency = end-start;
    return latency;

}
void cache_flush(char* va)
{
    __asm__ __volatile__
        (
         "clflush (%0);"
         :: "q" (va)
        );
    return ;
}
int main()
{
    unsigned long start, end;
    char c;
    int i;
    int index, status;
    char *p = 0;
    char probe[data * 4096];

    for(i = 0; i < data; i++)
    {
//        probe[i * 4096] = 0;
//        cache_flush(&probe[i * 4096]);
    }


    if(fork() == 0)
    {
//        *p = 1;
//        access();
        for(i = 0; i < data; i++)
            probe[i * 4096] = 0;
    }
    else
    {
        wait(&status);
        for(i = 0; i < data; i++)
        {
//            index = i * 4096;
//            start = time_stamp();
//            c = probe[i * 4096];
//            end = time_stamp();
            printf("%d. latency : %d\n", i, check_latency(&probe[i * 4096]));

        }
    }
}
