#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#define data 256
#define gran 4096
char probe_array[data * gran];

unsigned long time_stamp()
{
    unsigned long low, high;
    __asm__ __volatile__(
            "mfence\n\t"
            "rdtscp\n\t"
            "mfence\n\t"
            : "=a" (low), "=d" (high)
            );  
    return (low | high << 32);
}



void cache_flush(char* va) 
{
    __asm__ __volatile__(
            "clflush (%0);"
            :: "q" (va)
            );  

}
unsigned long check_read_latency(char *va)
{
    unsigned long start, end;
    start = time_stamp();
    __asm__ __volatile__
        (   

         //         "clflush (%0);"
         "mov (%0), %%r8;"
         "clflush (%0);"
         :: "q" (va)
        );  
    end = time_stamp();
    return end - start;
}
unsigned long check_write_latency(char *va)
{
    unsigned long start, end;
    start = time_stamp();
    __asm__ __volatile__
        (   

         //         "clflush (%0);"
         "movq $1, (%0);"
         "clflush (%0);"
         :: "q" (va)
        );  
    end = time_stamp();
    return end - start;
}

void access()
{
    int i;
    return ;
    for(i = 1; i < data; i++)
        probe_array[i * gran] = 0;
}
int main()
{
    int i, j = 0, k;
    unsigned long latency;
    while(1)
    {
        for(i = 0; i < data; i++)
        {
            latency = check_write_latency(&probe_array[i * gran]);
//            if(latency < 420)
                printf("%d. find it! - latency: %lu\n", i, latency);
        }
//        sleep(1);
    }


}
