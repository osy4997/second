#include "header.h"
char probe_array[gran * data];

static inline uint64_t ckGetCr3()
{

    uint64_t ret;

    __asm__ __volatile__
        (
         "xor %%rax, %%rax;"
         "retry:;"
         "mov %%cr3, %%rax;"
         "jz retry;"
//         "shl 
         : "=r"(ret)
        );
    return 1;
}

int main()
{
    int i;
    unsigned long latency;
    int pid;

    while(1)
    {
        pid = fork();
        printf("fork ...\n");
        if(pid == 0)
        {
            if(ckGetCr3() == 1)
            {
                printf("exit!\n");
                exit(0);
            }
        }
        else
            wait(0);
            
    }
}
