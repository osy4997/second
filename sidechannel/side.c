#include "side.h"
int main()
{
    char *ptr[16];
    pagemap = open("/proc/self/pagemap", O_RDONLY);
    char *addr = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

    int i, j = 1;
    for(i = 0; i < 16; i++)
    {
        ptr[i] = get_bank_addr(addr, i);
    }

    uint64_t start, end;


    while(1)
    {
        printf("%"PRIu64".   ", time_stamp());
        for(i = 0; i < 16; i++)
        {
            start = time_stamp();
            __asm__ __volatile__
                (
                 "mov (%0), %%r8;"
                 :: "q" (ptr[i])
                );
            end = time_stamp();
            
            __asm__ __volatile__
                (
                 "clflush (%0);"
                 :: "q" (ptr[i])
                );
                
            printf("%"PRIu64" ",end - start);
        }
        printf("\n");
        j++;
//        sleep(1);
//        usleep(1);
        
    }

    munmap(addr, SIZE);
}
