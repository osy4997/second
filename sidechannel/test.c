#include "side.h"
int main(int argc, char* argv[])
{
    char *ptr;
    pagemap = open("/proc/self/pagemap", O_RDONLY);
    char *addr = mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
    ptr = get_bank_addr(addr, atoi(argv[1]));
    uint64_t start, end;
    while(1)
    {
        printf("%"PRIu64". -", time_stamp());
        __asm__ __volatile__
            (
             "mov (%0), %%r8;"
             "mfence;"
             "clflush (%0);"
             :: "q" (ptr)
            );
        printf("\n");
    }
}
